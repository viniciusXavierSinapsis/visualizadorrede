Este projeto é uma prova de conceito para um visualizador de redes através da API do google maps.

Para executar, é necessário realizar os seguintes passos:

* Realizar a instalação do node ( https://nodejs.org/dist/v12.16.1/node-v12.16.1-x64.msi )
* Navegar via prompt de comando (cmd) até a pasta raiz do repositório e realizar o comando
```
#!bash

npm install better-sqlite3
```
*  Ainda na pasta raiz do projeto, utilizar o comando
```
#!bash

node script
```
para executar o script do projeto. (caso haja alguma outra aplicação com webservice hospedado, poderá gerar conflito, pois o script aloca portas de conexão http).

* Acesse o endereço http://localhost:80 para acessar a aplicação