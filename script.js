let http = require('http');
let fs = require("fs");
let url = require('url');
const db = require('better-sqlite3')('./' + fs.readdirSync('./').find(file => file.endsWith('.es')));
console.time('elapsed');
db.exec("ATTACH DATABASE './commons.cm' AS COMMONS")


let prepara = array => JSON.stringify(
	Object.values(array.reduce((obj, value) => {
		if (!obj[value.id]) obj[value.id] = [];
		obj[value.id][obj[value.id].length] = value;
		return obj;
	}, {}))
)
http.createServer(function (req, res) {
	fs.readFile('index.html', function (err, data) {
		res.writeHead(200, { 'Content-Type': 'text/html' });
		res.write(data);
		res.end();
	});
}).listen(80);

http.createServer(function (req, res) {
	res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
	res.write(prepara(
		db.prepare("select distinct latitude as lat, longitude as lng, id_rede_mt as rede, id_trecho_mt as id, id_barra as barra from (select latitude, longitude, id_rede_mt, id_trecho_mt, COMMONS.TB_BARRA.id_barra from tb_trecho_mt join COMMONS.TB_BARRA on ID_BARRA_1 = COMMONS.TB_BARRA.ID_BARRA union all select latitude, longitude, id_rede_mt, id_trecho_mt, COMMONS.TB_BARRA.id_barra from tb_trecho_mt join COMMONS.TB_BARRA on ID_BARRA_2 = COMMONS.TB_BARRA.ID_BARRA) tb")
			.all()));
	res.end()
}).listen(70);

http.createServer(function (req, res) {
	var q = url.parse(req.url, true).query;
	res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
	res.write(JSON.stringify(
		db.prepare("select distinct id_barra_1 as barra1, id_barra_2 as barra2, id_rede_mt from (select id_barra_1, id_barra_2, id_rede_mt from tb_trecho_mt join COMMONS.TB_BARRA on ID_BARRA_1 = COMMONS.TB_BARRA.ID_BARRA and COMMONS.TB_BARRA.latitude between ? and ? and COMMONS.TB_BARRA.longitude between ? and ? union all select id_barra_1, id_barra_2, id_rede_mt from tb_trecho_mt join COMMONS.TB_BARRA on ID_BARRA_2 = COMMONS.TB_BARRA.ID_BARRA and COMMONS.TB_BARRA.latitude between ? and ? and COMMONS.TB_BARRA.longitude between ? and ?) tb")
			.all(q.lat1, q.lat2, q.lng1, q.lng2, q.lat1, q.lat2, q.lng1, q.lng2)));
	res.end()
}).listen(170);

http.createServer(function (req, res) {
	let entity = "TRECHO_BT";
	var q = url.parse(req.url, true).query;
	res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
	res.write(JSON.stringify(
		db.prepare("select distinct id_barra_1 as barra1, id_barra_2 as barra2, ID_" + entity + "  as id from (select id_barra_1, id_barra_2, ID_" + entity + " from tb_" + entity + " join COMMONS.TB_BARRA TB_BARRA_1 on ID_BARRA_1 = TB_BARRA_1.ID_BARRA and TB_BARRA_1.latitude between ? and ? and TB_BARRA_1.longitude between ? and ? union all select id_barra_1, id_barra_2, ID_" + entity + " from tb_" + entity + " join COMMONS.TB_BARRA TB_BARRA_1 on ID_BARRA_2 = TB_BARRA_1.ID_BARRA and TB_BARRA_1.latitude between ? and ? and TB_BARRA_1.longitude between ? and ? union all select id_barra_1, id_barra_2, id_" + entity + " from tb_" + entity + " where id_barra_1 in (" + q.barras + ") or id_barra_2 in (" + q.barras + ")) tb")
			.all(q.lat1, q.lat2, q.lng1, q.lng2, q.lat1, q.lat2, q.lng1, q.lng2)));
	res.end()
}).listen(270);

http.createServer(function (req, res) {
	let entity = "CHAVE_MT";
	var q = url.parse(req.url, true).query;
	res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
	res.write(JSON.stringify(
		db.prepare("select distinct id_barra_1 as barra1, id_barra_2 as barra2, ID_" + entity + "  as id from (select id_barra_1, id_barra_2, ID_" + entity + " from tb_" + entity + " join COMMONS.TB_BARRA TB_BARRA_1 on ID_BARRA_1 = TB_BARRA_1.ID_BARRA and TB_BARRA_1.latitude between ? and ? and TB_BARRA_1.longitude between ? and ? union all select id_barra_1, id_barra_2, ID_" + entity + " from tb_" + entity + " join COMMONS.TB_BARRA TB_BARRA_1 on ID_BARRA_2 = TB_BARRA_1.ID_BARRA and TB_BARRA_1.latitude between ? and ? and TB_BARRA_1.longitude between ? and ? union all select id_barra_1, id_barra_2, id_" + entity + " from tb_" + entity + " where id_barra_1 in (" + q.barras + ") or id_barra_2 in (" + q.barras + ")) tb")
			.all(q.lat1, q.lat2, q.lng1, q.lng2, q.lat1, q.lat2, q.lng1, q.lng2)));
	res.end()
}).listen(370);

http.createServer(function (req, res) {
	let entity = "CHAVE_BT";
	var q = url.parse(req.url, true).query;
	res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
	res.write(JSON.stringify(
		db.prepare("select distinct id_barra_1 as barra1, id_barra_2 as barra2, ID_" + entity + "  as id from (select id_barra_1, id_barra_2, ID_" + entity + " from tb_" + entity + " join COMMONS.TB_BARRA TB_BARRA_1 on ID_BARRA_1 = TB_BARRA_1.ID_BARRA and TB_BARRA_1.latitude between ? and ? and TB_BARRA_1.longitude between ? and ? union all select id_barra_1, id_barra_2, ID_" + entity + " from tb_" + entity + " join COMMONS.TB_BARRA TB_BARRA_1 on ID_BARRA_2 = TB_BARRA_1.ID_BARRA and TB_BARRA_1.latitude between ? and ? and TB_BARRA_1.longitude between ? and ? union all select id_barra_1, id_barra_2, id_" + entity + " from tb_" + entity + " where id_barra_1 in (" + q.barras + ") or id_barra_2 in (" + q.barras + ")) tb")
			.all(q.lat1, q.lat2, q.lng1, q.lng2, q.lat1, q.lat2, q.lng1, q.lng2)));
	res.end()
}).listen(470);

http.createServer(function (req, res) {
	let entity = "RAMAL_LIGACAO";
	var q = url.parse(req.url, true).query;
	res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
	res.write(JSON.stringify(
		db.prepare("select distinct id_barra_1 as barra1, id_barra_2 as barra2, ID_" + entity + "  as id from (select id_barra_1, id_barra_2, ID_" + entity + " from tb_" + entity + " join COMMONS.TB_BARRA TB_BARRA_1 on ID_BARRA_1 = TB_BARRA_1.ID_BARRA and TB_BARRA_1.latitude between ? and ? and TB_BARRA_1.longitude between ? and ? union all select id_barra_1, id_barra_2, ID_" + entity + " from tb_" + entity + " join COMMONS.TB_BARRA TB_BARRA_1 on ID_BARRA_2 = TB_BARRA_1.ID_BARRA and TB_BARRA_1.latitude between ? and ? and TB_BARRA_1.longitude between ? and ? union all select id_barra_1, id_barra_2, id_" + entity + " from tb_" + entity + " where id_barra_1 in (" + q.barras + ") or id_barra_2 in (" + q.barras + ")) tb")
			.all(q.lat1, q.lat2, q.lng1, q.lng2, q.lat1, q.lat2, q.lng1, q.lng2)));
	res.end();
}).listen(570);


http.createServer(function (req, res) {
	let entity = "CARGA_MT";
	var q = url.parse(req.url, true).query;
	res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
	res.write(JSON.stringify(db.prepare("select COMMONS.TB_BARRA.id_barra as barra1, ID_" + entity + " as id from tb_" + entity + " join COMMONS.TB_BARRA on tb_" + entity + ".ID_BARRA = COMMONS.TB_BARRA.ID_BARRA and latitude between ? and ? and longitude between ? and ?")
		.all(q.lat1, q.lat2, q.lng1, q.lng2)));
	res.end();
}).listen(670);

http.createServer(function (req, res) {
	let entity = "CARGA_BT";
	var q = url.parse(req.url, true).query;
	res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
	res.write(JSON.stringify(db.prepare("select COMMONS.TB_BARRA.id_barra as barra1, ID_" + entity + " as id from tb_" + entity + " join COMMONS.TB_BARRA on tb_" + entity + ".ID_BARRA = COMMONS.TB_BARRA.ID_BARRA and latitude between ? and ? and longitude between ? and ?")
		.all(q.lat1, q.lat2, q.lng1, q.lng2)));
	res.end();
}).listen(770);

http.createServer(function (req, res) {
	var q = url.parse(req.url, true).query;
	res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
	res.write(JSON.stringify(db.prepare("select distinct id_barra_1 as barra1, id_barra_2 as barra2, id_barra_3 as barra3, ID_TRAFO_DISTRIBUICAO as id from (select id_barra_1, id_barra_2, id_barra_3, ID_TRAFO_DISTRIBUICAO from tb_trafo_distribuicao join COMMONS.TB_BARRA on ID_BARRA_1 = COMMONS.TB_BARRA.ID_BARRA and COMMONS.TB_BARRA.latitude between ? and ? and COMMONS.TB_BARRA.longitude between ? and ? union all select id_barra_1, id_barra_2, id_barra_3, ID_TRAFO_DISTRIBUICAO from tb_trafo_distribuicao join COMMONS.TB_BARRA on ID_BARRA_2 = COMMONS.TB_BARRA.ID_BARRA and COMMONS.TB_BARRA.latitude between ? and ? and COMMONS.TB_BARRA.longitude between ? and ? union all select id_barra_1, id_barra_2, id_barra_3, ID_TRAFO_DISTRIBUICAO from tb_trafo_distribuicao join COMMONS.TB_BARRA on ID_BARRA_3 = COMMONS.TB_BARRA.ID_BARRA and COMMONS.TB_BARRA.latitude between ? and ? and COMMONS.TB_BARRA.longitude between ? and ? union all select id_barra_1, id_barra_2, id_barra_3, id_trafo_distribuicao from tb_trafo_distribuicao where id_barra_1 in (" + q.barras + ") or id_barra_2 in (" + q.barras + ") or id_barra_3 in (" + q.barras + ")) tb")
		.all(q.lat1, q.lat2, q.lng1, q.lng2, q.lat1, q.lat2, q.lng1, q.lng2, q.lat1, q.lat2, q.lng1, q.lng2)));
	res.end();
}).listen(870);

http.createServer(function (req, res) {
	var q = url.parse(req.url, true).query;
	res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
	res.write(JSON.stringify(db.prepare("select id_barra as id, latitude as lat, longitude as lng from COMMONS.TB_BARRA where latitude between ? and ? and longitude between ? and ?")
		.all(q.lat1, q.lat2, q.lng1, q.lng2)));
	res.end();
}).listen(1070);


http.createServer(function (req, res) {
	var q = url.parse(req.url, true).query;
	res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
	res.write(JSON.stringify(db.prepare("select id_barra as barra1, latitude as lat, longitude as lng from COMMONS.TB_BARRA where id_barra in (" + q.barras + ")")
		.all()));
	res.end();
}).listen(2070);

http.createServer(function (req, res) {
	var q = url.parse(req.url, true).query;
	res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
	let tabela;
	switch (+q.tipo) {
		case 0:
			tabela = "BARRA";
			break;
		case 1:
			tabela = "TRECHO_MT";
			break;
		case 2:
			tabela = "TRECHO_BT";
			break;
		case 3:
			tabela = "CHAVE_MT";
			break;
		case 4:
			tabela = "CHAVE_BT";
			break;
		case 5:
			tabela = "RAMAL_LIGACAO";
			break;
		case 6:
			tabela = "CARGA_MT";
			break;
		case 7:
			tabela = "CARGA_BT";
			break;
		case 8:
			tabela = "TRAFO_DISTRIBUICAO";
			break;
	}
	if (q.tipo == 0) {
		res.write(JSON.stringify(db.prepare("select latitude as lat, longitude as lng from COMMONS.TB_BARRA where id_barra = ? LIMIT 1")
			.all(q.id)[0]));
	}
	else {
		res.write(JSON.stringify(db.prepare("select latitude as lat, longitude as lng from TB_" + tabela + " join COMMONS.TB_BARRA on TB_" + tabela + ".ID_BARRA" + (q.tipo == 6 || q.tipo == 7 ? "" : "_1") + " = COMMONS.TB_BARRA.ID_BARRA where id_" + tabela + " = ? LIMIT 1")
			.all(q.id)[0]) || "");
	}
	res.end();
}).listen(2743);


console.timeLog("elapsed");
